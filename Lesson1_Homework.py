"""
1.Task: Create two variables first=10, second=30.
Display the result of mathematical interaction (+, -, *, / etc.)
for these numbers.
"""
# 1
a = 10
b = 20
print("Task 1:")
print(a + b)
print(a - b)
print(a / b)
print(a * b)
print(a % b)
print(a ** b)
print((a-b)/2)

"""
2.Task: Create a variable and alternately write down in it the result of
comparing (<, > , ==, !=) the numbers from task 1.
Display the result of each comparison.
"""
# 2
print("\nTask 2:")
result = a < b
print(result)
result = a > b
print(result)
result = a == b
print(result)
result = a != b
print(result)

